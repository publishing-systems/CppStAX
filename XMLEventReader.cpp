/* Copyright (C) 2017-2024 Stephan Kreutzer
 *
 * This file is part of CppStAX.
 *
 * CppStAX is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * CppStAX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with CppStAX. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/XMLEventReader.cpp
 * @todo Check well-formedness: check if start and end tags match by having a
 *     stack of the current XPath tree location/level/hierarchy/position, and
 *     if only one root element, and if starting with an element, etc.
 * @author Stephan Kreutzer
 * @since 2017-08-24
  */

#include "XMLEventReader.h"
#include "StartElement.h"
#include "EndElement.h"
#include "Characters.h"
#include "Comment.h"
#include "QName.h"
#include "Attribute.h"
#include <string>
#include <memory>
#include <sstream>
#include <iomanip>
#include <cctype>

namespace cppstax
{

XMLEventReader::XMLEventReader(std::istream& aStream):
  m_aStream(aStream),
  m_bHasNextCalled(false)
{
    m_aEntityReplacementDictionary.insert(std::pair<std::string, std::string>("amp", "&"));
    m_aEntityReplacementDictionary.insert(std::pair<std::string, std::string>("lt", "<"));
    m_aEntityReplacementDictionary.insert(std::pair<std::string, std::string>("gt", ">"));
    m_aEntityReplacementDictionary.insert(std::pair<std::string, std::string>("apos", "'"));
    m_aEntityReplacementDictionary.insert(std::pair<std::string, std::string>("quot", "\""));

    /** @todo Load more from a catalogue, which itself is written in XML and needs to be read
      * in here by another local XMLEventReader object, containing mappings from entity to
      * replacement characters. No need to deal with DTDs as they're non-XML, and extracting
      * those mappings from a DTD can be a separate program or manual procedure. Also see
      * intermediate workaround method XMLEventReader::addToEntityReplacementDictionary(). */
}

XMLEventReader::~XMLEventReader()
{

}

bool XMLEventReader::hasNext()
{
    if (m_aEvents.size() > 0)
    {
        return true;
    }

    if (m_bHasNextCalled == true)
    {
        return false;
    }
    else
    {
        m_bHasNextCalled = true;
    }

    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        return false;
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte == '<')
    {
        return HandleTag();
    }
    else
    {
        return HandleText(cByte);
    }
}

std::unique_ptr<XMLEvent> XMLEventReader::nextEvent()
{
    if (m_aEvents.size() <= 0 &&
        m_bHasNextCalled == false)
    {
        if (hasNext() != true)
        {
            throw new std::logic_error("Attempted XMLEventReader::nextEvent() while there isn't one instead of checking XMLEventReader::hasNext() first.");
        }
    }

    m_bHasNextCalled = false;

    if (m_aEvents.size() <= 0)
    {
        throw new std::logic_error("XMLEventReader::nextEvent() while there isn't one, ignoring XMLEventReader::hasNext() == false.");
    }

    std::unique_ptr<XMLEvent> pEvent = std::move(m_aEvents.front());
    m_aEvents.pop();

    return pEvent;
}

int XMLEventReader::addToEntityReplacementDictionary(const std::string& strName, const std::string& strReplacementText)
{
       if (strName == "amp" ||
           strName == "lt" ||
           strName == "gt" ||
           strName == "apos" ||
           strName == "quot")
       {
           throw new std::invalid_argument("Redefinition of built-in entity.");
       }

       m_aEntityReplacementDictionary[strName] = strReplacementText;
       return 0;
}

bool XMLEventReader::HandleTag()
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Tag incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte == '?')
    {
        if (HandleProcessingInstruction() == true)
        {
            return true;
        }
        else
        {
            m_bHasNextCalled = false;
            return hasNext();
        }
    }
    else if (cByte == '/')
    {
        return HandleTagEnd();
    }
    else if (cByte == '!')
    {
        return HandleMarkupDeclaration();
    }
    else if (std::isalpha(cByte, m_aLocale) == true ||
             cByte == '_')
    {
        return HandleTagStart(cByte);
    }
    else
    {
        unsigned int nByte(cByte);
        std::stringstream aMessage;
        aMessage << "Unknown byte '" << cByte << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") within element.";
        throw new std::runtime_error(aMessage.str());
    }
}

bool XMLEventReader::HandleTagStart(const char& cFirstByte)
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Tag start incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    std::unique_ptr<std::string> pNamePrefix(nullptr);
    std::unique_ptr<std::string> pNameLocalPart(new std::string());
    std::unique_ptr<std::list<std::unique_ptr<Attribute>>> pAttributes(new std::list<std::unique_ptr<Attribute>>);

    pNameLocalPart->push_back(cFirstByte);

    do
    {
        /** @todo Check, if special characters appear at the very start where not allowed.
          * This might apply for the namespace prefix as well as for the element name. */

        if (cByte == ':')
        {
            if (pNamePrefix != nullptr)
            {
                throw new std::runtime_error("There can't be two prefixes in element name.");
            }

            pNamePrefix = std::move(pNameLocalPart);
            pNameLocalPart.reset(new std::string());
        }
        else if (cByte == '>')
        {
            if (pNamePrefix == nullptr)
            {
                pNamePrefix.reset(new std::string);
            }

            std::unique_ptr<QName> pName(new QName("", *pNameLocalPart, *pNamePrefix));
            std::unique_ptr<StartElement> pStartElement(new StartElement(std::move(pName), std::move(pAttributes)));
            std::unique_ptr<XMLEvent> pEvent(new XMLEvent(std::move(pStartElement),
                                                          nullptr,
                                                          nullptr,
                                                          nullptr,
                                                          nullptr));
            m_aEvents.push(std::move(pEvent));
            break;
        }
        else if (cByte == '/')
        {
            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                throw new std::runtime_error("Tag start incomplete.");
            }

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte != '>')
            {
                throw new std::runtime_error("Empty start + end tag end without closing '>'.");
            }

            if (pNamePrefix == nullptr)
            {
                pNamePrefix.reset(new std::string);
            }

            std::unique_ptr<QName> pName(new QName("", *pNameLocalPart, *pNamePrefix));
            std::unique_ptr<StartElement> pStartElement(new StartElement(std::move(pName), std::move(pAttributes)));
            std::unique_ptr<XMLEvent> pEvent(new XMLEvent(std::move(pStartElement),
                                                          nullptr,
                                                          nullptr,
                                                          nullptr,
                                                          nullptr));
            m_aEvents.push(std::move(pEvent));

            pName = std::unique_ptr<QName>(new QName("", *pNameLocalPart, *pNamePrefix));
            std::unique_ptr<EndElement> pEndElement(new EndElement(std::move(pName)));
            pEvent = std::unique_ptr<XMLEvent>(new XMLEvent(nullptr,
                                                            std::move(pEndElement),
                                                            nullptr,
                                                            nullptr,
                                                            nullptr));
            m_aEvents.push(std::move(pEvent));

            break;
        }
        else if (std::isspace(cByte, m_aLocale) != 0)
        {
            if (pNameLocalPart->length() <= 0)
            {
                throw new std::runtime_error("Start tag name begins with whitespace.");
            }

            while (true)
            {
                m_aStream.get(cByte);

                if (m_aStream.eof() == true)
                {
                    throw new std::runtime_error("Tag start incomplete.");
                }

                if (m_aStream.bad() == true)
                {
                    throw new std::runtime_error("Stream is bad.");
                }

                if (cByte == '>')
                {
                    break;
                }
                else if (std::isspace(cByte, m_aLocale) != 0)
                {
                    // Ignore/consume.
                }
                else if (cByte == '/')
                {
                    m_aStream.unget();

                    if (m_aStream.bad() == true)
                    {
                        throw new std::runtime_error("Stream is bad.");
                    }

                    break;
                }
                else
                {
                    HandleAttributes(cByte, pAttributes);
                    break;
                }
            }
        }
        else if (std::isalnum(cByte, m_aLocale) == true ||
                 cByte == '-' ||
                 cByte == '_' ||
                 cByte == '.')
        {
            pNameLocalPart->push_back(cByte);
        }
        else
        {
            unsigned int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ") not supported in a start tag name.";
            throw new std::runtime_error(aMessage.str());
        }

        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Tag start incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

    } while (true);

    return true;
}

bool XMLEventReader::HandleTagEnd()
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Tag end incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    std::unique_ptr<std::string> pNamePrefix(nullptr);
    std::unique_ptr<std::string> pNameLocalPart(new std::string());

    // No validity check for the XML element name is needed
    // if end tags are compared to start tags and the start
    // tags were already checked.

    bool bComplete = false;

    do
    {
        if (cByte == ':')
        {
            if (pNamePrefix != nullptr)
            {
                throw new std::runtime_error("There can't be two prefixes in the element name.");
            }

            pNamePrefix = std::move(pNameLocalPart);
            pNameLocalPart.reset(new std::string());
        }
        else if (cByte == '>')
        {
            if (pNamePrefix == nullptr)
            {
                pNamePrefix.reset(new std::string);
            }

            std::unique_ptr<QName> pName(new QName("", *pNameLocalPart, *pNamePrefix));
            std::unique_ptr<EndElement> pEndElement(new EndElement(std::move(pName)));
            std::unique_ptr<XMLEvent> pEvent(new XMLEvent(nullptr,
                                                          std::move(pEndElement),
                                                          nullptr,
                                                          nullptr,
                                                          nullptr));
            m_aEvents.push(std::move(pEvent));

            bComplete = true;
            break;
        }
        else if (std::isalnum(cByte, m_aLocale) == true ||
                 cByte == '-' ||
                 cByte == '_' ||
                 cByte == '.')
        {
            pNameLocalPart->push_back(cByte);
        }
        else
        {
            unsigned int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ") not supported in an end tag name.";
            throw new std::runtime_error(aMessage.str());
        }

        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("End tag incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

    } while (true);

    if (bComplete != true)
    {
        throw new std::runtime_error("End tag incomplete.");
    }

    return true;
}

bool XMLEventReader::HandleText(const char& cFirstByte)
{
    std::unique_ptr<std::string> pData(new std::string);

    if (cFirstByte == '&')
    {
        std::unique_ptr<std::string> pResolvedText = nullptr;

        ResolveEntity(pResolvedText);
        pData->append(*pResolvedText);
    }
    else
    {
        pData->push_back(cFirstByte);
    }

    char cByte = '\0';

    while (true)
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            break;
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '<')
        {
            m_aStream.unget();

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            break;
        }
        else if (cByte == '&')
        {
            std::unique_ptr<std::string> pResolvedText = nullptr;

            ResolveEntity(pResolvedText);
            pData->append(*pResolvedText);
        }
        else
        {
            pData->push_back(cByte);
        }
    }

    std::unique_ptr<Characters> pCharacters(new Characters(std::move(pData)));
    std::unique_ptr<XMLEvent> pEvent(new XMLEvent(nullptr,
                                                  nullptr,
                                                  std::move(pCharacters),
                                                  nullptr,
                                                  nullptr));
    m_aEvents.push(std::move(pEvent));

    return true;
}

bool XMLEventReader::HandleProcessingInstruction()
{
    std::unique_ptr<std::string> pTarget = nullptr;

    HandleProcessingInstructionTarget(pTarget);

    if (pTarget == nullptr)
    {
        throw new std::runtime_error("Processing instruction without target name.");
    }

    if (pTarget->length() == 3)
    {
        if ((pTarget->at(0) == 'x' ||
             pTarget->at(0) == 'X') &&
            (pTarget->at(1) == 'm' ||
             pTarget->at(1) == 'M') &&
            (pTarget->at(2) == 'l' ||
             pTarget->at(2) == 'L'))
        {
            /** @todo This should read the XML declaration instructions instead
              * of just consuming/ignoring it. */

            char cByte('\0');
            int nMatchCount = 0;

            while (nMatchCount < 2)
            {
                m_aStream.get(cByte);

                if (m_aStream.eof() == true)
                {
                    throw new std::runtime_error("XML declaration incomplete.");
                }

                if (m_aStream.bad() == true)
                {
                    throw new std::runtime_error("Stream is bad.");
                }

                if (cByte == '>')
                {
                    if (nMatchCount >= 1)
                    {
                        return false;
                    }
                    else
                    {
                        nMatchCount = 0;
                    }
                }
                else if (cByte == '?')
                {
                    nMatchCount = 1;
                }
                else
                {
                    nMatchCount = 0;
                }
            }

            return false;
        }
    }

    std::unique_ptr<std::string> pData(new std::string);
    char cByte('\0');
    int nMatchCount = 0;

    while (nMatchCount < 2)
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Processing instruction data incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '>')
        {
            if (nMatchCount >= 1)
            {
                std::unique_ptr<ProcessingInstruction> pProcessingInstruction(new ProcessingInstruction(std::move(pTarget), std::move(pData)));
                std::unique_ptr<XMLEvent> pEvent(new XMLEvent(nullptr,
                                                              nullptr,
                                                              nullptr,
                                                              nullptr,
                                                              std::move(pProcessingInstruction)));
                m_aEvents.push(std::move(pEvent));

                return true;
            }
            else
            {
                pData->push_back('>');
                nMatchCount = 0;
            }
        }
        else if (cByte == '?')
        {
            if (nMatchCount > 0)
            {
                pData->push_back('?');
            }

            nMatchCount = 1;
        }
        else
        {
            if (nMatchCount > 0)
            {
                pData->push_back('?');
            }

            nMatchCount = 0;

            pData->push_back(cByte);
        }
    }

    return false;
}

bool XMLEventReader::HandleProcessingInstructionTarget(std::unique_ptr<std::string>& pTarget)
{
    std::unique_ptr<std::string> pName = nullptr;
    char cByte('\0');
    int nMatchCount = 0;

    while (nMatchCount < 2)
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Processing instruction target name incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '?' &&
            nMatchCount <= 0)
        {
            nMatchCount++;
        }
        else if (cByte == '>' &&
                 nMatchCount <= 1)
        {
            throw new std::runtime_error("Processing instruction ended before processing instruction target name could be read.");
        }
        else if (std::isspace(cByte, m_aLocale) != 0)
        {
            if (nMatchCount > 0)
            {
                throw new std::runtime_error("Processing instruction target name interrupted by '?'.");
            }

            if (pName == nullptr)
            {
                throw new std::runtime_error("Processing instruction without target name.");
            }

            pTarget = std::move(pName);

            return true;
        }
        else
        {
            if (nMatchCount > 0)
            {
                throw new std::runtime_error("Processing instruction target name interrupted by '?'.");
            }

            if (pName == nullptr)
            {
                if (std::isalpha(cByte, m_aLocale) != true)
                {
                    unsigned int nByte(cByte);
                    std::stringstream aMessage;
                    aMessage << "Character '" << cByte << "' (0x"
                               << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                               << ") not supported as first character of a processing instruction target name.";
                    throw new std::runtime_error(aMessage.str());
                }

                pName = std::unique_ptr<std::string>(new std::string);
            }

            pName->push_back(cByte);
        }
    }

    return false;
}

bool XMLEventReader::HandleMarkupDeclaration()
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Markup declaration incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Steam is bad.");
    }

    if (cByte == '-')
    {
        return HandleComment();
    }
    else
    {
        /*
         * Keeping it pure XML.

        do
        {
            if (cByte == '<')
            {
                throw new std::runtime_error("Markup declaration type not implemented.");
            }
            else if (cByte == '>')
            {
                m_bHasNextCalled = false;
                return hasNext();
            }
            else
            {
                // Consume/ignore/skip.
            }

            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                throw new std::runtime_error("Markup declaration incomplete.");
            }

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

        } while (true);
        */

        throw new std::runtime_error("Markup declaration type not implemented.");
    }

    return true;
}

bool XMLEventReader::HandleComment()
{
    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Comment incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte != '-')
    {
        throw new std::runtime_error("Comment malformed.");
    }

    std::unique_ptr<std::string> pData(new std::string);

    unsigned int nMatchCount = 0;
    const unsigned int END_SEQUENCE_LENGTH = 3;
    char cEndSequence[END_SEQUENCE_LENGTH] = { '-', '-', '>' };

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Comment incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == cEndSequence[nMatchCount])
        {
            if (nMatchCount + 1 < END_SEQUENCE_LENGTH)
            {
                ++nMatchCount;
            }
            else
            {
                std::unique_ptr<Comment> pComment(new Comment(std::move(pData)));
                std::unique_ptr<XMLEvent> pEvent(new XMLEvent(nullptr,
                                                              nullptr,
                                                              nullptr,
                                                              std::move(pComment),
                                                              nullptr));
                m_aEvents.push(std::move(pEvent));

                break;
            }
        }
        else
        {
            if (nMatchCount > 0)
            {
                // Instead of strncpy() and C-style char*.
                for (unsigned int i = 0; i < nMatchCount; i++)
                {
                    pData->push_back(cEndSequence[i]);
                }

                pData->push_back(cByte);
                nMatchCount = 0;
            }
            else
            {
                pData->push_back(cByte);
            }
        }

    } while (true);

    return true;
}

bool XMLEventReader::HandleAttributes(const char& cFirstByte, std::unique_ptr<std::list<std::unique_ptr<Attribute>>>& pAttributes)
{
    if (pAttributes == nullptr)
    {
        throw new std::invalid_argument("nullptr passed.");
    }

    std::unique_ptr<QName> pAttributeName(nullptr);
    std::unique_ptr<std::string> pAttributeValue(nullptr);

    HandleAttributeName(cFirstByte, pAttributeName);
    HandleAttributeValue(pAttributeValue);

    pAttributes->push_back(std::unique_ptr<Attribute>(new Attribute(std::move(pAttributeName), std::move(pAttributeValue))));

    char cByte('\0');

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Tag start incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '>')
        {
            // Not part of the attributes any more and indicator for outer
            // methods to complete the StartElement.
            m_aStream.unget();

            break;
        }
        else if (cByte == '/')
        {
            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                throw new std::runtime_error("Tag start incomplete.");
            }

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte != '>')
            {
                throw new std::runtime_error("Empty start + end tag end without closing '>'.");
            }

            m_aStream.unget();

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            m_aStream.unget();

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            break;
        }
        else if (std::isspace(cByte, m_aLocale) != 0)
        {
            // Ignore/consume.
            continue;
        }
        else
        {
            HandleAttributeName(cByte, pAttributeName);
            HandleAttributeValue(pAttributeValue);

            pAttributes->push_back(std::unique_ptr<Attribute>(new Attribute(std::move(pAttributeName), std::move(pAttributeValue))));
        }

    } while (true);

    return true;
}

bool XMLEventReader::HandleAttributeName(const char& cFirstByte, std::unique_ptr<QName>& pName)
{
    std::unique_ptr<std::string> pNamePrefix(nullptr);
    std::unique_ptr<std::string> pNameLocalPart(new std::string());

    char cByte(cFirstByte);

    if (cByte == ':')
    {
        pNamePrefix.reset(new std::string());
    }
    else if (std::isalnum(cByte, m_aLocale) == true ||
             cByte == '_')
    {
        pNameLocalPart->push_back(cByte);
    }
    else
    {
        unsigned int nByte(cByte);
        std::stringstream aMessage;
        aMessage << "Character '" << cByte << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") not supported as first character of an attribute name.";
        throw new std::runtime_error(aMessage.str());
    }

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Attribute name incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ':')
        {
            if (pNamePrefix != nullptr)
            {
                throw new std::runtime_error("There can't be two prefixes in attribute name.");
            }

            pNamePrefix = std::move(pNameLocalPart);
            pNameLocalPart.reset(new std::string());
        }
        else if (std::isspace(cByte, m_aLocale) != 0)
        {
            cByte = ConsumeWhitespace();

            if (cByte == '\0')
            {
                throw new std::runtime_error("Attribute incomplete.");
            }
            else if (cByte != '=')
            {
                throw new std::runtime_error("Attribute name is malformed.");
            }

            // To make sure that the next loop iteration will end up in cByte == '='.
            m_aStream.unget();

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }
        }
        else if (cByte == '=')
        {
            if (pNamePrefix == nullptr)
            {
                pNamePrefix.reset(new std::string);
            }

            pName = std::unique_ptr<QName>(new QName("", *pNameLocalPart, *pNamePrefix));

            return true;
        }
        else if (std::isalnum(cByte, m_aLocale) == true ||
                 cByte == '-' ||
                 cByte == '_' ||
                 cByte == '.')
        {
            pNameLocalPart->push_back(cByte);
        }
        else
        {
            unsigned int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ") not supported in an attribute name.";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);

    return false;
}

bool XMLEventReader::HandleAttributeValue(std::unique_ptr<std::string>& pValue)
{
    pValue = std::unique_ptr<std::string>(new std::string);
    char cDelimiter(ConsumeWhitespace());

    if (cDelimiter == '\0')
    {
        throw new std::runtime_error("Attribute is missing its value.");
    }
    else if (cDelimiter != '\'' &&
             cDelimiter != '"')
    {
        unsigned int nByte(cDelimiter);
        std::stringstream aMessage;
        aMessage << "Attribute value doesn't start with a delimiter like ''' or '\"', instead, '" << cDelimiter << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") was found.";
        throw new std::runtime_error(aMessage.str());
    }

    char cByte('\0');

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Attribute value incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == cDelimiter)
        {
            return true;
        }
        else if (cByte == '&')
        {
            std::unique_ptr<std::string> pResolvedText = nullptr;

            ResolveEntity(pResolvedText);
            pValue->append(*pResolvedText);
        }
        else
        {
            pValue->push_back(cByte);
        }

    } while (true);

    return false;
}

void XMLEventReader::ResolveEntity(std::unique_ptr<std::string>& pResolvedText)
{
    if (pResolvedText != nullptr)
    {
        throw new std::invalid_argument("No nullptr passed.");
    }

    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Entity incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte == ';')
    {
        throw new std::runtime_error("Entity has no name.");
    }
    else if (cByte == '#')
    {
        ResolveEntityCharacter(pResolvedText);
        return;
    }
    else
    {
        std::unique_ptr<std::string> pEntityName(new std::string);
        pEntityName->push_back(cByte);

        do
        {
            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                throw new std::runtime_error("Entity incomplete.");
            }

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == ';')
            {
                break;
            }

            pEntityName->push_back(cByte);

        } while (true);

        std::map<std::string, std::string>::iterator iter = m_aEntityReplacementDictionary.find(*pEntityName);

        if (iter != m_aEntityReplacementDictionary.end())
        {
            pResolvedText = std::unique_ptr<std::string>(new std::string(iter->second));
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unable to resolve entity '&" << *pEntityName << ";'.";
            throw new std::runtime_error(aMessage.str());
        }
    }
}

void XMLEventReader::ResolveEntityCharacter(std::unique_ptr<std::string>& pResolvedText)
{
    if (pResolvedText != nullptr)
    {
        throw new std::invalid_argument("No nullptr passed.");
    }

    char cByte = '\0';
    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Character entity incomplete.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte == ';')
    {
        throw new std::runtime_error("Character entity has no number.");
    }

    std::unique_ptr<std::string> pEntityNumber(new std::string);
    bool bIsHex = false;

    if (cByte == 'x')
    {
        bIsHex = true;
    }
    else
    {
        if (std::isdigit(cByte) == 0)
        {
            unsigned int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Character entity contains a non-number '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

        pEntityNumber->push_back(cByte);
    }

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Character entity incomplete.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ';')
        {
            break;
        }

        if (std::isdigit(cByte) == 0)
        {
            if (bIsHex == true)
            {
                if (cByte != 'A' &&
                    cByte != 'B' &&
                    cByte != 'C' &&
                    cByte != 'D' &&
                    cByte != 'E' &&
                    cByte != 'F' &&
                    cByte != 'a' &&
                    cByte != 'b' &&
                    cByte != 'c' &&
                    cByte != 'd' &&
                    cByte != 'e' &&
                    cByte != 'f')
                {
                    unsigned int nByte(cByte);
                    std::stringstream aMessage;
                    aMessage << "Hexadecimal character entity contains an invalid character '" << cByte << "' (0x"
                             << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                             << ").";
                    throw new std::runtime_error(aMessage.str());
                }
            }
            else
            {
                unsigned int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Character entity contains a non-number '" << cByte << "' (0x"
                         << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                         << ").";
                throw new std::runtime_error(aMessage.str());
            }
        }

        pEntityNumber->push_back(cByte);

    } while (true);

    if (pEntityNumber->length() <= 0)
    {
        throw new std::runtime_error("Character entity has no number.");
    }

    std::stringstream aConverter;

    if (bIsHex == true)
    {
        aConverter << std::hex;
    }

    unsigned long nCodePoint = 0UL;

    aConverter << *pEntityNumber;
    aConverter >> nCodePoint;

    if (aConverter.fail() != false)
    {
        std::stringstream aMessage;
        aMessage << "Conversion of character entity \"";

        if (bIsHex == true)
        {
            aMessage << "0x";
        }

        aMessage << *pEntityNumber << "\" failed.";
        throw new std::runtime_error(aMessage.str());
    }

    std::deque<unsigned char> aBytes;

    // Relying on nCodePoint being unsigned for nCodePoint >= 0x00UL.
    if (nCodePoint <= 0x7FUL)
    {
        // ASCII.
        // 0???????
        aBytes.push_front(nCodePoint & 0x7FUL);
    }
    else
    {
        // Less significant bytes into aBytes, each starting with 10??????,
        // filling in 6 bits from nCodePoint in the order of their reverse
        // nCodePoint source occurrence, while it's clear it needs to be at
        // least two bytes into aBytes because the nCodePoint didn't fit into
        // ASCII 0???????. With ANSI/"codepage" bit 1??????? turned on and
        // followed by a reserved signaller ?0??????, the overflow/carry-over
        // to the next byte-block is in steps of 6 bits each, so in comparison
        // to the source nCodePoint 8-bit bytes, 2 bits are lost/reserved/extra
        // per block for Unicode encoding/signalling.
        //
        // See man utf-8(7) for details.
        //
        // The first code point number exceeding ASCII of 0x80 with 10|000000
        // gets its lower 6 bits filled into the less-significant byte prefixed
        // with the Unicode block signaller, and the higher two bits get filled
        // into the next block, then there causing the loss/reservation of one
        // usable bit (as there would be no ??????01 in the higher byte, the
        // overflow/carry-over would at minimum lead to ??????10), as such
        // was added to the less-significant bit for signalling/prefixing.
        //
        // The last (reverse from nCodePoint source) most significant byte into
        // aBytes starts with a number of 1-bits in the amount of the count of
        // the Unicode byte blocks that follow this first output byte/block,
        // so if the result in total will have 2 bytes, then it's one less
        // significant byte, therefore the first byte block starts with
        // 1???????. If the total is 3 bytes, the two less significant bytes
        // get indicated by 11?????? in the first most significant byte in
        // the result. That count of 1-bits is followed by the block signaller
        // *10*, so a total of 2 bytes has its most significant byte set up
        // as 110?????, one with 3 bytes has its most significant byte set up
        // as 1110????, and so on. The remaining bits get filled up with the
        // remaining bits/carry-over/overflow that are left from nCodePoints.

        /** @attention This might currently not support endian, both within
          * a byte and the order of bytes? */

        // Less significant byte(s):

        // According to the scheme, in theory, if it were a single byte (and
        // no special handling of ASCII), the maximum value for the single
        // byte/block would be XX111111 or 0x3F with the XX prefixed as
        // 10?????? block signaller. Because there's special handling for
        // ASCII above and the value in nCodePoint didn't fit into it, it's
        // clear it'll be at least two bytes, and the less significant
        // byte/block will at minimum make full use of the 6 bits in the
        // block. To account for the scheme however, the "remaining" 6 bits
        // or 0x3F for the theoretically "pure" single first byte need to
        // be part of the processing in the initial first loop/byte as well,
        // even if the will inevitably later be a second more-significant
        // byte implied, as the first block fit with its capacity of 6 bits
        // XX?????? takes them off the value of nCodePoint already.
        // nRemainingMostSignificantBitValue keeps track if there would be
        // fit into the most significant byte/block if one were to be
        // constructed now, or if another loop iteration is needed to add
        // another less significant block/byte (then progressing
        // nRemainingMostSignificantBitValue to only 5 bits XXX????? remaining,
        // then to 4 bits remaining, and so on). Each loop reduces the
        // nCodePoint value by a sequence of 6 bits.
        unsigned long nRemainingMostSignificantBitValue = 0x3FUL;

        while (nCodePoint > nRemainingMostSignificantBitValue)
        {
            // 00??????
            unsigned char cCharacter = nCodePoint & 0x3FUL;
            // 10??????
            cCharacter |= 0x80UL;

            aBytes.push_front(cCharacter);
            nCodePoint >>= 6;

            if (nCodePoint == 0UL)
            {
                break;
            }

            nRemainingMostSignificantBitValue >>= 1;

            if (nRemainingMostSignificantBitValue == 0UL || nRemainingMostSignificantBitValue > 0x3FUL)
            {
                throw new std::runtime_error("Unicode codepoint character entity exceeds the maximum of 6 bytes.");
            }
        }

        if (aBytes.size() > 5)
        {
            throw new std::runtime_error("Unicode codepoint character entity exceeds the maximum of 6 bytes.");
        }

        // Most significant byte:

        // 00000001
        unsigned long nMaskTarget = 0x1UL;
        unsigned char cCharacter = '\0';
        unsigned int nBitCount = 0U;

        do
        {
            ++nBitCount;

            if ((nCodePoint & 0x1UL) == 0x1UL)
            {
                cCharacter |= nMaskTarget;
            }

            nMaskTarget <<= 1;
            nCodePoint >>= 1;

        } while (nCodePoint > 0x0UL);

        // 2UL for *10*.
        if (aBytes.size() + 2UL + nBitCount > 8)
        {
            throw new std::runtime_error("Unicode codepoint character entity exceeds the maximum of 6 bytes.");
        }

        // 10000000
        nMaskTarget = 0x80UL;

        // 10??????
        unsigned char cCharacterPrefix = 0x80U;
        // 11??????
        unsigned long nMask = 0xC0U;

        for (std::size_t i = 0U; i < aBytes.size(); i++)
        {
            cCharacterPrefix >>= 1;
            cCharacterPrefix |= nMaskTarget;

            nMask >>= 1;
            nMask |= nMaskTarget;
        }

        // Leave prefix untouched, mark/mask 
        nMask = ~nMask;

        cCharacterPrefix |= (cCharacter & nMask);

        aBytes.push_front(cCharacterPrefix);
    }

    pResolvedText = std::unique_ptr<std::string>(new std::string);

    for (std::deque<unsigned char>::const_iterator iter = aBytes.begin();
         iter != aBytes.end();
         iter++)
    {
        *pResolvedText += *iter;
    }
}

/**
 * @retval Returns the first non-whitespace character or '\0' in
 *     case of end-of-file.
 */
char XMLEventReader::ConsumeWhitespace()
{
    char cByte('\0');

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            return '\0';
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isspace(cByte, m_aLocale) == 0)
        {
            return cByte;
        }

    } while (true);
}

}
